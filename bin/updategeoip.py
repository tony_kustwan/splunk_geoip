#!/usr/bin/python

import sys
import os


dir_path = os.path.dirname(os.path.realpath(__file__))

#just incase URLLIB package is not on the host system
#if your system has the URLLIB installed you can remove this section.
##############################################################
PACKAGE_PATH=dir_path+"/packages"   #OPTIONAL: remove if OS has URLLIB
sys.path.append(PACKAGE_PATH)       #OPTIONAL: remove if OS has URLLIB
import urllib
##############################################################

TA_GEOIP_DEST='$SPLUNK_HOME/etc/shcluster/apps/TA-GEOIP/geoip_db'

#CHECK TO SEE IF TA-GEOIP is on a SHCLUSTER
if os.path.exists(os.path.expandvars(TA_GEOIP_DEST)):
    print "%s exists!" % (TA_GEOIP_DEST)
else:
    print "could find %s - trying non-shcluster directory" % (TA_GEOIP_DEST)
    TA_GEOIP_DEST='$SPLUNK_HOME/etc/apps/TA-GEOIP/geoip_db'
    if os.path.exists(os.path.expandvars(TA_GEOIP_DEST)):
        print "%s exists!" % (TA_GEOIP_DEST)
    else:
        print "Could not find TA-GEOIP, Please locate TA-GEOIP found in ./bin/ADD-ON/"
        sys.exit(0)

os.chdir(os.path.expandvars(TA_GEOIP_DEST))

urllib.urlretrieve("http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz","GeoLite2-City-Latest.mmdb.gz")

os.system("gunzip -f GeoLite2-City-Latest.mmdb.gz")
os.system("chmod 644 GeoLite2-City-Latest.mmdb")