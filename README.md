# My project's README
This App comes with 2 Splunk App's

1. SPLUNK_GEOIP
Purpose: The purpose of this app is to schedule the retrival of the Latest MaxMind GEO IP DB

2. TA-GEOIP
Location: ./bin/ADD-ON/TA-GEOIP
Purpose: The purpose of this app is the store the gGeoLite2-City-Latest.mmdb Database


INSTALL INSTRUCTION:

NON-Search HEAD CLUSTER:  normal install

SEARH HEAD CLUSTER:

Install SPLUNK_GEOIP on the deployer in $SPLUNK_HOME/etc/apps

Install TA-GEOIP on the deployer $SPLUNK_HOME/etc/shcluster/apps